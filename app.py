from flask import Flask
from flask import request
import logging

app = Flask(__name__)
handler = logging.FileHandler('flask.log', encoding='UTF-8')
handler.setLevel(logging.DEBUG)
logging_format = logging.Formatter('%(asctime)s - %(levelname)s - %(filename)s - %(funcName)s - %(lineno)s - %(message)s')
handler.setFormatter(logging_format)
app.logger.addHandler(handler)


@app.route('/<string:name>')
def hello(name):
    print(name)
    print(request.remote_addr)
    print(request.headers.get('User-Agent'))
    return '[{}]Hello World!{}'.format(
        name, request.remote_addr)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
